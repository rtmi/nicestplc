package main

import (
	"log"
	"syscall/js"
)

var beforeUnloadCh = make(chan struct{})

func main() {

	// Submit button handler
	var cb js.Func
	cb = js.FuncOf(playMix)
	defer cb.Release()
	js.Global().Get("document").Call("getElementById", "submit").Call("addEventListener", "click", cb)

	// Define a new callback to run Go logic.
	var beforeUnloadCb js.Func
	beforeUnloadCb = js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		// Signal the channel so Go program can end.
		beforeUnloadCh <- struct{}{}
		beforeUnloadCb.Release()
		return nil
	})
	// Attach the callback to the webview's unload hook.
	addEventListener := js.Global().Get("addEventListener")
	addEventListener.Invoke("beforeunload", beforeUnloadCb)


	// Keep Go program alive until the webview closes.
	<-beforeUnloadCh
	// todo clean-up left?

}

func playMix(this js.Value, args []js.Value) interface{} {
	log.Println("button clicked")

	inp := js.Global().Get("document").Call("getElementById", "mixid")
	mid := inp.Get("value").String()
	log.Printf("Mix ID - %v", mid)

	return nil
}
