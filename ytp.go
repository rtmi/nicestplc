/*  try to be consistent with 
https://github.com/PierfrancescoSoffritti/android-youtube-player#youtubeplayerlistener
*/

package main

import (
	"bytes"
	"html/template"
	"log"
	"syscall/js"
)

type Specifier struct {
	VID          string `json:"id"`
	StartSeconds int    `json:"start"`
	EndSeconds   int    `json:"end"`
}

// a) the http server has to catch the yt route with the Mix ID "path"
// b) then build the HTML by appending templates
// c) the HTML loads the main.wasm to do event handler logic

//TODO support override via YouTubePlayerListener argument
func Initialize() {
	var err error
	var ytphtml *template.Template

	//TODO this system call can't be wasm-side/js
	ytphtml, err = template.ParseFiles("ytp.html")
	if err != nil {
		log.Fatal(err)
	}

	spec := struct {
		Playlist []Specifier
	}{
		[]Specifier{},	//DEBUG
	}

	buf := new(bytes.Buffer)

	err = ytphtml.Execute(buf, spec)
	if err != nil {
		log.Fatal(err)
	}


	log.Printf("DEBUG - %v", buf.String())
	// todo attach result to DOM
	js.Global().Get("document.body").Call("appendChild", buf.String())
}

