// step 5. Can we load two iframes in the webview to play the music track from one

package main

import (
	"encoding/json"
	"flag"
	"html/template"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	"net/http"
	"time"

	"github.com/zserge/webview"
)

type Specifier struct {
	VID         string `json:"id"`
	PlaySeconds int    `json:"seconds,omitempty"`
}
type Message struct {
	Data []Specifier `json:"data"`
}

var indexhtml *template.Template
var cat []Specifier

func main() {
	catpath := flag.String("catpath", "catalog.json", "File of Youtube video ID list")

	flag.Parse()
	var err error

	cat, err = loadCatalog(*catpath)
	if err != nil {
		panic(err)
	}

	indexhtml, err = template.ParseFiles("audio.tmpl")
	if err != nil {
		log.Fatal(err)
	}

	ln, err2 := net.Listen("tcp", "127.0.0.1:0")
	if err2 != nil {
		log.Fatal(err2)
	}
	defer ln.Close()
	go func() {
		http.HandleFunc("/", handler)
		log.Fatal(http.Serve(ln, nil))
	}()
	webview.Open("TheNicestPlace", "http://"+ln.Addr().String(), 490, 280, false)
}

func handler(w http.ResponseWriter, r *http.Request) {
	shuf := shuffleVID(cat)

	spec := struct {
		Playlist    []string
		PlaySeconds int
	}{
		shuf,
		6000,
	}
	err := indexhtml.Execute(w, spec)
	if err != nil {
		log.Fatal(err)
	}
}

func shuffleVID(catalog []Specifier) []string {
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	r1.Shuffle(len(catalog), func(i, j int) {
		catalog[i], catalog[j] = catalog[j], catalog[i]
	})

	// catalog.reduce(x => x.VID)

	vsm := make([]string, len(catalog))
	for i, v := range catalog {
		vsm[i] = v.VID
	}
	return vsm
}

func loadCatalog(path string) ([]Specifier, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		log.Printf("JSON unmarshall error %v", err)
		return nil, err
	}

	var m Message
	err = json.Unmarshal(b, &m)
	if err != nil {
		log.Printf("JSON unmarshall error %v", err)
		return nil, err
	}

	return m.Data, nil
}
