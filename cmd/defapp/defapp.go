package main

import (
	"flag"
	"github.com/pkg/browser"
	"html/template"
	"io"
	"log"
)

type Specifier struct {
	VID         string
	PlaySeconds int
}

func main() {
	vid := flag.String("vid", "M7lc1UVf-VE", "Youtube video ID")
	seconds := flag.Int("seconds", 6000, "Seconds to play")
	flag.Parse()

	spec := Specifier{*vid, *seconds}
	tmpl, err := template.ParseFiles("player.tmpl")
	if err != nil {
		panic(err)
	}

	pr, pw := io.Pipe()

	go func() {
		defer pw.Close()
		err := tmpl.Execute(pw, spec)
		if err != nil {
			log.Fatal(err)
		}
	}()

	if err := browser.OpenReader(pr); err != nil {
		log.Fatal(err)
	}
}

// To queue vid, modify the state-change js to retrieve the next item from (?firebase)
// (?pouchdb)
