// step 3. Given a list of NPI VIDs, shuffle and play

package main

import (
	"encoding/json"
	"flag"
	"github.com/pkg/browser"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"time"
)

type Specifier struct {
	VID         string `json:"id"`
	PlaySeconds int    `json:"seconds,omitempty"`
}
type Message struct {
	Data []Specifier `json:"data"`
}

func main() {
	catpath := flag.String("catpath", "catalog.json", "File of Youtube video ID list")
	seconds := flag.Int("seconds", 6000, "Seconds to play")
	flag.Parse()

	cat, err := loadCatalog(*catpath)
	if err != nil {
		panic(err)
	}

	r := shuffleVID(cat)

	spec := struct {
		Playlist    []string
		PlaySeconds int
	}{
		r,
		*seconds,
	}
	tmpl, err := template.ParseFiles("playlist.tmpl")
	if err != nil {
		panic(err)
	}

	pr, pw := io.Pipe()

	go func() {
		defer pw.Close()
		err := tmpl.Execute(pw, spec)
		if err != nil {
			log.Fatal(err)
		}
	}()

	if err := browser.OpenReader(pr); err != nil {
		log.Fatal(err)
	}
}

func shuffleVID(catalog []Specifier) []string {
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	r1.Shuffle(len(catalog), func(i, j int) {
		catalog[i], catalog[j] = catalog[j], catalog[i]
	})

	// catalog.reduce(x => x.VID)

	vsm := make([]string, len(catalog))
	for i, v := range catalog {
		vsm[i] = v.VID
	}
	return vsm
}

func loadCatalog(path string) ([]Specifier, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		log.Printf("JSON unmarshall error %v", err)
		return nil, err
	}

	var m Message
	err = json.Unmarshal(b, &m)
	if err != nil {
		log.Printf("JSON unmarshall error %v", err)
		return nil, err
	}

	return m.Data, nil
}
