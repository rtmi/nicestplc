// step 6. Extend specifier JSON mix playlist to fit within the music track
//
// so it appears that we need to use the player.cueVideoById Youtube API
// which means we will loop through a array of specifiers
// (this is approaching the trade-off of a go-wrapper around the JS player)

package main

import (
	"encoding/json"
	"flag"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	"net/http"
	"time"

	"github.com/pkg/browser"
	"github.com/zserge/webview"
)

type Specifier struct {
	VID          string `json:"id"`
	StartSeconds int    `json:"start"`
	EndSeconds   int    `json:"end"`
}

var indexhtml *template.Template
var cat []Specifier

func main() {
	catpath := flag.String("catpath", "catalog.json", "File of Youtube video ID list")
	debugmode := flag.Bool("debug", false, "Render in default browser to debug")

	flag.Parse()
	var err error

	cat, err = loadCatalog(*catpath)
	if err != nil {
		panic(err)
	}

	indexhtml, err = template.ParseFiles("trim.tmpl")
	if err != nil {
		log.Fatal(err)
	}

	if *debugmode {
		debugLaunch(cat)
		return
	}
	ln, err2 := net.Listen("tcp", "127.0.0.1:0")
	if err2 != nil {
		log.Fatal(err2)
	}
	defer ln.Close()
	go func() {
		http.HandleFunc("/", handler)
		log.Fatal(http.Serve(ln, nil))
	}()
	webview.Open("TheNicestPlace", "http://"+ln.Addr().String(), 490, 280, false)
}

func handler(w http.ResponseWriter, r *http.Request) {
	shuf := shuffleVID(cat)

	spec := struct {
		Playlist []Specifier
	}{
		shuf[0:4],
	}
	err := indexhtml.Execute(w, spec)
	if err != nil {
		log.Fatal(err)
	}
}

func debugLaunch(catalog []Specifier) {
	shuf := shuffleVID(catalog)

	// Limit the size to 4 to align with the length of the background music
	spec := struct {
		Playlist []Specifier
	}{
		shuf[0:4],
	}

	pr, pw := io.Pipe()

	go func() {
		defer pw.Close()
		err := indexhtml.Execute(pw, spec)
		if err != nil {
			log.Fatal(err)
		}
	}()

	if err := browser.OpenReader(pr); err != nil {
		log.Fatal(err)
	}
}

func shuffleVID(catalog []Specifier) []Specifier {
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	r1.Shuffle(len(catalog), func(i, j int) {
		catalog[i], catalog[j] = catalog[j], catalog[i]
	})

	return catalog

	// catalog.reduce(x => x.VID)
	/*
		vsm := make([]string, len(catalog))
		for i, v := range catalog {
			vsm[i] = v.VID
		}
		return vsm	*/
}

func loadCatalog(path string) ([]Specifier, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		log.Printf("JSON unmarshall error %v", err)
		return nil, err
	}

	m := struct {
		Data []Specifier `json:"data"`
	}{}
	err = json.Unmarshal(b, &m)
	if err != nil {
		log.Printf("JSON unmarshall error %v", err)
		return nil, err
	}

	return m.Data, nil
}
